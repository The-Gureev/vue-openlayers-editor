import Transform from 'ol-ext/interaction/Transform';
import { shiftKeyOnly } from 'ol/events/condition';
import { Style, Fill, Stroke } from 'ol/style';
import Map from 'ol/Map';

// Стили рисования
const style: Style = new Style({
    stroke: new Stroke({ width:2, color: [18, 18, 18, 0.9], lineDash: [10, 10] }),
    fill: new Fill({ color: [18, 18, 18, 0.9]}),
});

const interaction: Transform = new Transform({
    enableRotatedTransform: false,
    addCondition: shiftKeyOnly,
    hitTolerance: 2,
    translateFeature: true,
    scale: true,
    rotate: true,
    keepAspectRatio: undefined,
    keepRectangle: false,
    translate: true,
    stretch: true,
    // Get scale on points
    pointRadius: function (f) {
        const radius = f.get('radius') || 1;
        return [radius, radius];
    }
});


const draggableInit = (map: Map) => {
    if (!map) return;
    // interaction.setDefaultStyle();
    interaction.setStyle('default', style);
    //interaction.setDefaultStyle();
    map.addInteraction(interaction);
    interaction.setActive(true);
    interaction.set('translate', interaction.get('translate'));
};

const draggableRemove = (map: Map): void => {
    if (!map) return;

    map.getInteractions().forEach(function (interaction) {
        if (interaction instanceof Transform) {
            map.removeInteraction(interaction);
        }
    });
    interaction.setActive(false);
    interaction.set('translate', null);
};

export {
    draggableInit,
    draggableRemove
}