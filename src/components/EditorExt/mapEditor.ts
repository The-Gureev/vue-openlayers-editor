
import { Draw } from 'ol/interaction';
import { Style, Fill, Stroke } from 'ol/style';
import DrawHole from 'ol-ext/interaction/DrawHole';
import VectorSource from 'ol/source/Vector';
import VectorSourceType from 'ol/source/Vector'
import VectorLayer from 'ol/layer/Vector';
import Polygon from 'ol/geom/Polygon';
import { Type } from 'ol/geom/Geometry';
// import * as DrawHole from '/ol-ext/interaction/DrawHole';
import Map from 'ol/Map';


let draw: Draw;
let drawHole: DrawHole;
// let type: Type;
// let snap: Snap;
// let modify: Modify;
let drawMap = false;

// Слой для рисования
const drawLayer: VectorLayer<VectorSourceType<Polygon>> = new VectorLayer({
    source: new VectorSource(),
    properties: { name: 'drawLayer' }
});

// Стили рисования
const style: Style = new Style({
    fill: new Fill({
        color: 'rgba(255, 255, 255, 0.8)'
    }),
    stroke: new Stroke({
        color: 'blue',
        width: 1
    })
});

const editorStart = (map: Map): void => {
    if (!map) return;
    map.addLayer(drawLayer);
}

// Создать редактор
const editorInit = (map: Map, type: Type): void => {
    const source: VectorSource<Polygon> | undefined = drawLayer.getSource() || undefined;

    if(!drawMap) {
        drawMap = true;
        editorStart(map);
    }

    draw = new Draw({
        source,
        type,
        style
    });

    // Отверстия
    drawHole = new DrawHole({
        layers: [drawLayer],
        type,
    });

    drawHole.setActive(false);

    // snap = new Snap({ source: source });
    console.log('map ', map);
    map.addInteraction(draw);
    // map.addInteraction(snap);
    map.addInteraction(drawHole);
}


const changeEditorType = (map: Map, type: Type): void => {
    editorRemove(map);
    editorInit(map, type);
}


// Удалить редактор
const editorRemove = (map: Map): void => {
    if (!map) return;
    map.removeInteraction(draw);


    let interactionEvent: Draw | null = null;
    let interactionEventHole: DrawHole | null = null;

    map.getInteractions().forEach(function (interaction) {
        interaction instanceof Draw ? interactionEvent = interaction : '';
        interaction instanceof DrawHole ? interactionEventHole = interaction : '';
    });

    draw.setActive(false);
    drawHole.setActive(false);
    interactionEvent ? map.removeInteraction(interactionEvent) : '';
    interactionEventHole ? map.removeInteraction(interactionEventHole) : '';
}

const setHole = (active: boolean): void => {
    draw.setActive(!active);
    drawHole.setActive(active);
}

export {
    editorStart,
    changeEditorType,
    editorInit,
    editorRemove,
    setHole
}