import UiButton from './uiButton.vue';
import UiCheckbox from './uiCheckbox.vue';

export {
    UiCheckbox,
    UiButton
}