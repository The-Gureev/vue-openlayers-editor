import { createApp } from 'vue'
import App from './App.vue'
import Vue3ColorPicker from "vue3-colorpicker";
import "vue3-colorpicker/style.css";
import './registerServiceWorker'

createApp(App).component('Vue3ColorPicker', Vue3ColorPicker).mount('#app')
