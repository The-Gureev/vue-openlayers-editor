import Map from 'ol/Map';
import { Vector as VectorSource } from 'ol/source.js';
import { ref, Ref } from 'vue';

const map: Ref<Map | undefined> = ref<Map>();
const source: Ref<VectorSource | undefined> = ref<VectorSource>();


const setMap = (mapObject: Map): void => {
    if (!mapObject) return;
    map.value = mapObject;
}

const setSource = (sourceObject: VectorSource): void => {
    source.value = sourceObject;
}

export {
    setSource,
    setMap,

    source,
    map
}